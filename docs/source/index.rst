.. ZFC Biomass Mapping documentation master file, created by
   sphinx-quickstart on Mon Feb 18 10:55:53 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Biomass mapping with L-band radar
=================================

.. image:: ./images/radar_backscatter.png
    :width: 400pt
    :align: center 
	
Remote sensing in African woodlands is unusually difficult: woodlands and forest are very heterogeneous, trees and grasses have strong and unpredictable phenological effects, and fires occur frequently. Together these can render standard methods of land cover/use/change mapping with optical data ineffective. However, there are opportunities in freely available long-wavelength radar data for mapping forest properties, bypassing many of these obstacles.

Long wavelength radar data are a long-established method of generating maps of forest AGB in woodlands and dry forests. Until recently such data has been expensive, but free data are now available from JAXA’s ALOS-1 and ALOS-2 L-band sensors (2007 – 2010, 2015 – onwards), which offer a relatively straightforward means to generate maps of AGB in woodlands and forests with AGB of up to around 150 tonnes per hectare.

In this tutorial we'll run through an example of using freely available L-band radar mosaics to produce biomass and biomass change maps in Zimbabwe.

Contents
========

.. toctree::
   :maxdepth: 1
   :numbered:
   
   theory.rst
   qgis_setup.rst
   biomass.rst
   change.rst
   biota.rst
   what_next.rst

