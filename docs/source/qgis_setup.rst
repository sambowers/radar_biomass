QGIS Setup
==========

.. NOTE:: 
    If you have already installed QGIS, you can skip this section.

What is QGIS
------------

QGIS is a desktop Geographic Information System (GIS) application that supports viewing, editing and analysis of geospatial data. QGIS is free and open source, and offers a viable alternative to commercial software such as ArcGIS.

We'll be using QGIS to visualise and manipulate radar data from ALOS-1.

Downloading QGIS
-------------------

You can download QGIS from the `QGIS website <https://www.qgis.org/en/site/forusers/download.html>`_. Make sure you choose the correct version, which is **Long term release, QGIS Standalone Installer** for Windows. Most users will require the 64-bit version, but if you're have an older laptop you might need to download the 32-bit version.

If you’re struggling to find the correct version, you can also download it (Windows, 64-bit) directly `here <https://qgis.org/downloads/QGIS-OSGeo4W-2.18.28-1-Setup-x86_64.exe>`_ (392 MB). 

.. NOTE::
    Are you using Linux or Mac, you may need to download a different version to that above. If accessing this large volume of data is a problem, we'll bring along copies of QGIS to the workshop for you to install.
    

Installing QGIS
---------------

Once the download is complete, navigate to your ``Downloads`` folder, and run the executable named ``QGIS-OSGeo4W-2.18.28-1-Setup-x86_64.exe`` (or similar).

This will open the QGIS installer. You can accept all default settings. Click Next:

.. image:: ./images/qgis_1.PNG
    :width: 300pt
    :align: center 

Then 'I Agree':

.. image:: ./images/qgis_2.PNG
    :width: 300pt
    :align: center 

Then Next:

.. image:: ./images/qgis_3.PNG
    :width: 300pt
    :align: center 

Then Install:

.. image:: ./images/qgis_4.PNG
    :width: 300pt
    :align: center 

...and wait until the installation is complete:

.. image:: ./images/qgis_5.PNG
    :width: 300pt
    :align: center 

Press Finish to complete the installation.

.. image:: ./images/qgis_6.PNG
    :width: 300pt
    :align: center 

Running QGIS
------------

To run QGIS, navigate to your install through the start menu, and Run ``QGIS Desktop 2.18.28`` or similar.

If your installation is successful, you should see an interface that looks something like the following:

.. image:: ./images/qgis_7.PNG
    :width: 500pt
    :align: center 

Installation problems?
----------------------

If something isn't working for you, please feel free to get in touch with sam.bowers@ed.ac.uk.

